unit Table;
{$mode objfpc}{$H+}
interface
 uses crt,inputdata;
 const
   nocolortext=$7;
 procedure tabv             ( xx:integer; yy:integer;sizerectangle:integer);
 procedure LastLine         (var line:integer;var xx:integer);
 procedure CenterLine       (var line:integer;var xx:integer);
 procedure PrimaryLine      ();
 procedure CenterLineMenu   ();
 procedure RightLine        ();
 procedure UpAndDownLine    ();
 procedure FrameTableData   (var n:integer;var line:integer;var xx:integer);
 procedure MainTableData    (var n:integer;var pos:integer;var line:integer;var xx:integer;var jj:integer);
 procedure MenuTab          ();
 procedure MunuTab2         (var quantity:integer;var PrimaryPosX:integer;var PrimaryPosY:integer;var UpperLeftCorner:char;var LowerRightCorner:char;var DownLeftCorner:char;var UpperRightCorner:char);
 procedure Tablee2End       (var FullLine:integer;var PrimaryPosY:integer;var PrimaryPosX:integer);
 procedure Table2InitialLine(var FullLine:integer;var quantity:integer;var PrimaryPosX:integer;var PrimaryPosY:integer;var UpperLeftCorner:char;var LowerRightCorner:char;var DownLeftCorner:char;var UpperRightCorner:char);
 procedure Table2MidleLine  (var quantity:integer;var PrimaryPosX:integer;var PrimaryPosY:integer;var UpperLeftCorner:char;var LowerRightCorner:char;var DownLeftCorner:char;var UpperRightCorner:char);
 procedure MenuTabInsert2   (xx:integer;yy:integer;sizerectangle:integer);
implementation
/////////////////////////////////'�R�///////////////////////////////////////////////
{��楤�� ��� ��祭�� ���㣮�쭨���}
procedure Tabv ( xx:integer; yy:integer;sizerectangle:integer);
var
  i:integer;
  begin
    textcolor(red);
    for i:=1 to sizerectangle do
      begin
        gotoxy(xx+i,yy);
        write(#205);
        gotoxy(xx+i,yy+2);
        write(#205);
      end;
      gotoxy(xx,yy+1);
      write(#186);
      gotoxy(xx+sizerectangle,yy+1);
      write(#186);
      gotoxy(xx+sizerectangle,yy);
      write(#187);
      gotoxy(xx+sizerectangle,yy+2);
      write(#188);
      gotoxy(xx,yy);
      write(#201);
      gotoxy(xx,yy+2);
      write(#200);
  end;
/////////////////////////////??���� ���<�///////////////////////////////////////////////////

procedure LastLine(var line:integer;var xx:integer);
var
  i:integer;
begin
  for i:=1 to 63 do
    begin
      gotoxy (1+i,Line);
      write  (#205);
    end;
    gotoxy (1,Line-1);
    write  (#186);
    gotoxy (1,Line);
    write  (#200);
    gotoxy (64,Line-1);
    write  (#186);
    gotoxy (64,Line);
    write  (#188);
    gotoxy (7,Line-1);
    write  (#186);
    gotoxy (21,Line-1);
    write  (#186);
    gotoxy (35,Line-1);
    write  (#186);
    gotoxy (49,Line-1);
    write  (#186);
    gotoxy (7,Line);
    write  (#202);
    gotoxy (21,Line);
    write  (#202);
    gotoxy (35,Line);
    write  (#202);
    gotoxy (49,Line);
    write  (#202);
  end;

procedure CenterLine(var line:integer;var xx:integer);
var
i:integer;
  begin
    for i:=1 to 63 do
      begin
        gotoxy (1+i,line);
        write  (#205);
      end;
      gotoxy  (7,line-1);
      write   (#186);
      gotoxy  (21,line-1);
      write   (#186);
      gotoxy  (35,line-1);
      write   (#186);
      gotoxy  (49,line-1);
      write   (#186);
      gotoxy  (64,line-1);
      write   (#186);
      gotoxy  (1,line-1);
      write   (#186);
      gotoxy  (1,Line);
      write   (#204);
      gotoxy  (7,Line);
      write   (#206);
      gotoxy  (21,Line);
      write   (#206);
      gotoxy  (35,Line);
      write   (#206);
      gotoxy  (49,Line);
      write   (#206);
      gotoxy  (64,Line);
      write   (#185);
      line:=line+2;
  end;

procedure PrimaryLine();
var
   i:integer;
begin
  for i:=1 to 63 do
    begin
      gotoxy(1+i,2);
      write(#205);
      gotoxy(1+i,4);
      write(#205);
    end;
    gotoxy  (1,2);
    write  (#201);
    gotoxy (1,4);
    write  (#204);
    gotoxy (64,2);
    write  (#187);
    gotoxy (64,4);
    write  (#185);
    gotoxy (1,3) ;
    write  (#186);
    gotoxy (64,3);
    write  (#186);
    gotoxy (2,3);
    TextColor(green);
    write  ('�����');
    TextAttr:=nocolortext;
    gotoxy (7,3);
    write  (#186);
    gotoxy (8,3);
    TextColor(green);
    write  ('������');
    TextAttr:=nocolortext;
    gotoxy (21,3);
    write  (#186);
    gotoxy (22,3);
    TextColor(green);
    write  ('����� ������');
    TextAttr:=nocolortext;
    gotoxy (35,3);
    write  (#186);
    gotoxy (36,3);
    TextColor(green);
    write  ('���-�� �������');
    TextAttr:=nocolortext;
    gotoxy (49,3);
    write  (#186);
    gotoxy (50,3);
    TextColor(green);
    write  ('  ⨯  ');
    TextAttr:=nocolortext;
    gotoxy (7,2);
    write  (#203);
    gotoxy (21,2);
    write  (#203);
    gotoxy (35,2);
    write  (#203);
    gotoxy (49,2);
    write  (#203);
    gotoxy (7,4);
    write  (#206);
    gotoxy (21,4);
    write  (#206);
    gotoxy (35,4);
    write  (#206);
    gotoxy (49,4);
    write  (#206);
end;
procedure CenterLineMenu();
var
   i,j:integer;
   b:boolean;
begin
  for i:=1 to 17 do
    begin
      b:=odd(i);
      if b=true then
        begin
          gotoxy(11,4+i);
          writeln(#186);
        end
       else
         begin
           gotoxy(11,4+i);
           writeln(#204);
           for j:=1 to 31 do
             begin
               gotoxy(11+j,4+i);
               write(#205);
             end;
         end;
    end;
end;
procedure RightLine();
var
    i:integer;
    b:boolean;
begin
  for i:=1 to 17 do
    begin
      begin
        b:=odd(i);
        if b=true then
          begin
            gotoxy(43,4+i);
            writeln(#186);
          end
         else
           begin
             gotoxy(43,4+i);
             writeln(#185);
           end;
      end;
      gotoxy(11,22);
      writeln(#200);
      gotoxy(43,4);
      writeln(#187);
      gotoxy(43,22);
      writeln(#188);
    end;
end;
procedure UpAndDownLine();
var
    I:integer;
begin
  for i:=1 to 31 do
    begin
      gotoxy(11+i,4);
      writeln(#205);
      gotoxy(11+i,22);
      write(#205);
    end;
    gotoxy(11,4);
    writeln(#201);
end;
procedure FrameTableData (var n:integer;var line:integer;var xx:integer);
var
    j:integer;
begin
  gotoxy(30,1);
  write('��ࢠ� ⠡���');
  for j:=1 to n do
    begin
      if j=1 then
        begin
          primaryline();
          lastline(line,xx);
        end
       else
         begin
         if j<>1 then
           begin
             centerline(line,xx);
             if (j=7) or(j=n ) then
               begin
                 lastline(line,xx);
               end;
           end;
         end;
    end;
end;
procedure MainTableData (var n:integer;var pos:integer ;var line:integer;var xx:integer;var jj:integer);
begin
  if jj>7 then n:=7
   else n:=jj;
    if jj<>0 then
      begin
        FrameTableData ( n,line,xx);
      end
     else
       begin
         gotoxy (35,9);
         write ('��� ������');
         gotoxy (15,11);
         write ('������ ���� ������� ��� ��室� � ������� ����');
         readkey
       end;
end;
procedure MenuTab ();
var xx,yy,sizerectangle:integer;
begin
////////////////////////////////////////////////////////////////////////////////
  textcolor(6);
  gotoxy(9,3);
  write('������');
  xx:=4;
  yy:=4;
  sizerectangle:=15;
  tabv(xx,yy,sizerectangle);
////////////////////////////////////////////////////////////////////////////////
  textcolor(6);
  gotoxy(46,3);
  write('����� ������');
  xx:=44 ;
  yy:=4;
  sizerectangle:=15;
  tabv (xx,yy,sizerectangle);
////////////////////////////////////////////////////////////////////////////////
  textcolor(6);
  gotoxy(5,13);
  write('���-�� �������');
  xx:=4;
  yy:=14;
  sizerectangle:=15;
  tabv (xx,yy,sizerectangle);
////////////////////////////////////////////////////////////////////////////////
  textcolor(6);
  gotoxy(47,13);
  write('  ⨯  ');
  xx:=44;
  yy:=14;
  sizerectangle:=15;
  tabv (xx,yy,sizerectangle);
////////////////////////////////////////////////////////////////////////////////
  textcolor(6);
  gotoxy(25,20);
  xx:=23;
  yy:=19;
  sizerectangle:=15;
  tabv (xx,yy,sizerectangle);
////////////////////////////////////////////////////////////////////////////////
  textcolor(7);
end;
procedure MunuTab2(var quantity:integer;var PrimaryPosX:integer;var PrimaryPosY:integer;var UpperLeftCorner:char;var LowerRightCorner:char;var DownLeftCorner:char;var UpperRightCorner:char);
var
  x,i:integer;
begin
  x:=201;
  for i:=1 to quantity do
    begin
      gotoxy(PrimaryPosX+i,PrimaryPosY);
      write(#205);
    end;
    gotoxy (PrimaryPosX+1,PrimaryPosY+1);
    write('1');
    gotoxy(PrimaryPosX,PrimaryPosY+1) ;
    write(#186) ;
    gotoxy((PrimaryPosX+quantity)+1,PrimaryPosY+1);
    write(#186) ;
    gotoxy(PrimaryPosX,PrimaryPosY);
    write(UpperLeftCorner);
    gotoxy(PrimaryPosX+quantity+1,PrimaryPosY);
    write(UpperRightCorner);
    PrimaryPosX:=PrimaryPosX+quantity+1;
end;
procedure Tablee2End(var FullLine:integer;var PrimaryPosY:integer;var PrimaryPosX:integer);
var
  J:integer;
begin
  gotoxy(1,1);
  write(FullLine);
  PrimaryPosX:=1;
  for j:=1 to FullLine do
    begin
      gotoxy(PrimaryPosX,PrimaryPosY);
      write(#200);
      gotoxy(PrimaryPosX+FullLine+1,PrimaryPosY);
      write(#188);
      gotoxy(PrimaryPosX+j,PrimaryPosY);
      write(#205);
      gotoxy(7,PrimaryPosY);
      write(#202);
      gotoxy(23,PrimaryPosY);
      write(#202);
    end;
end;
procedure Table2InitialLineLeft(var FullLine:integer;var quantity:integer;var PrimaryPosX:integer;var PrimaryPosY:integer;var UpperLeftCorner:char;var LowerRightCorner:char;var DownLeftCorner:char;var UpperRightCorner:char);
begin
  UpperLeftCorner:=(#201);
           LowerRightCorner:=(#206);
           DownLeftCorner:=(#204);
           UpperRightCorner:=(#203);
           PrimaryPosX:=1;
           PrimaryPosY:=PrimaryPosY;
           quantity:=5;
           MunuTab2(quantity,PrimaryPosX,PrimaryPosY,UpperLeftCorner,LowerRightCorner,DownLeftCorner,UpperRightCorner);
           FullLine:=FullLine+quantity;
end;
procedure Table2InitialLineMidle(var FullLine:integer;var quantity:integer;var PrimaryPosX:integer;var PrimaryPosY:integer;var UpperLeftCorner:char;var LowerRightCorner:char;var DownLeftCorner:char;var UpperRightCorner:char);
begin
  UpperLeftCorner:=(#203);
           LowerRightCorner:=(#206);
           DownLeftCorner:=(#204);
           UpperRightCorner:=(#203);
           PrimaryPosY:=PrimaryPosY;
           quantity:=15;
           MunuTab2(quantity,PrimaryPosX,PrimaryPosY,UpperLeftCorner,LowerRightCorner,DownLeftCorner,UpperRightCorner);
           FullLine:=FullLine+quantity;
end;
procedure Table2InitialLineRight(var FullLine:integer;var quantity:integer;var PrimaryPosX:integer;var PrimaryPosY:integer;var UpperLeftCorner:char;var LowerRightCorner:char;var DownLeftCorner:char;var UpperRightCorner:char);
begin
  UpperLeftCorner:=(#203);
           LowerRightCorner:=(#206);
           DownLeftCorner:=(#204);
           UpperRightCorner:=(#187);
           PrimaryPosY:=PrimaryPosY;
           quantity:=15;
           MunuTab2(quantity,PrimaryPosX,PrimaryPosY,UpperLeftCorner,LowerRightCorner,DownLeftCorner,UpperRightCorner);
           FullLine:=FullLine+quantity+2;
end;
procedure Table2InitialLine(var FullLine:integer;var quantity:integer;var PrimaryPosX:integer;var PrimaryPosY:integer;var UpperLeftCorner:char;var LowerRightCorner:char;var DownLeftCorner:char;var UpperRightCorner:char);
var
  j:integer;
begin
  for j:=1 to 3 do
     begin
       if J=1 then
         begin
           Table2InitialLineLeft(FullLine,quantity,PrimaryPosX,PrimaryPosY,UpperLeftCorner,LowerRightCorner,DownLeftCorner,UpperRightCorner);
         end;
       if j=2 then
         begin
           Table2InitialLineMidle(FullLine,quantity,PrimaryPosX,PrimaryPosY,UpperLeftCorner,LowerRightCorner,DownLeftCorner,UpperRightCorner);
         end;
       if j=3 then
         begin
           Table2InitialLineRight(FullLine,quantity,PrimaryPosX,PrimaryPosY,UpperLeftCorner,LowerRightCorner,DownLeftCorner,UpperRightCorner);
         end;
     end;
  PrimaryPosY:=PrimaryPosY+2
end;
procedure Table2MidleLineLeft(var quantity:integer;var PrimaryPosX:integer;var PrimaryPosY:integer;var UpperLeftCorner:char;var LowerRightCorner:char;var DownLeftCorner:char;var UpperRightCorner:char);
begin
  UpperLeftCorner:=(#204);
           LowerRightCorner:=(#206);
           DownLeftCorner:=(#204);
           UpperRightCorner:=(#206);
           PrimaryPosX:=1;
           PrimaryPosY:=PrimaryPosY;
           quantity:=5;
           MunuTab2(quantity,PrimaryPosX,PrimaryPosY,UpperLeftCorner,LowerRightCorner,DownLeftCorner,UpperRightCorner);
end;
procedure Table2MidleLineMidle(var quantity:integer;var PrimaryPosX:integer;var PrimaryPosY:integer;var UpperLeftCorner:char;var LowerRightCorner:char;var DownLeftCorner:char;var UpperRightCorner:char);
begin
  UpperLeftCorner:=(#206);
           LowerRightCorner:=(#206);
           DownLeftCorner:=(#204);
           UpperRightCorner:=(#206);
           PrimaryPosY:=PrimaryPosY;
           quantity:=15;
           MunuTab2(quantity,PrimaryPosX,PrimaryPosY,UpperLeftCorner,LowerRightCorner,DownLeftCorner,UpperRightCorner);
end;
procedure Table2MidleLineRight(var quantity:integer;var PrimaryPosX:integer;var PrimaryPosY:integer;var UpperLeftCorner:char;var LowerRightCorner:char;var DownLeftCorner:char;var UpperRightCorner:char);
begin
  UpperLeftCorner:=(#206);
           LowerRightCorner:=(#206);
           DownLeftCorner:=(#204);
           UpperRightCorner:=(#185);
           PrimaryPosY:=PrimaryPosY;
           quantity:=15;
           MunuTab2(quantity,PrimaryPosX,PrimaryPosY,UpperLeftCorner,LowerRightCorner,DownLeftCorner,UpperRightCorner);
         end;
procedure Table2MidleLine (var quantity:integer;var PrimaryPosX:integer;var PrimaryPosY:integer;var UpperLeftCorner:char;var LowerRightCorner:char;var DownLeftCorner:char;var UpperRightCorner:char);
var
  j:integer;
begin
  for j:=1 to 3 do
     begin
       if J=1 then
         begin
           Table2MidleLineLeft(quantity,PrimaryPosX,PrimaryPosY,UpperLeftCorner,LowerRightCorner,DownLeftCorner,UpperRightCorner);
         end;
       if j=2 then
         begin
           Table2MidleLineMidle(quantity,PrimaryPosX,PrimaryPosY,UpperLeftCorner,LowerRightCorner,DownLeftCorner,UpperRightCorner);
         end;
       if j=3 then
         begin
           Table2MidleLineRight(quantity,PrimaryPosX,PrimaryPosY,UpperLeftCorner,LowerRightCorner,DownLeftCorner,UpperRightCorner);
         end;
     end;
  PrimaryPosY:=PrimaryPosY+2;
end;
{��஥ ���� ��� �����}
procedure MenuTabInsert2( xx:integer;yy:integer;sizerectangle:integer);
begin
  textcolor(6);
  gotoxy(9,3);
  write('��������');
  xx:=4;
  yy:=4;
  sizerectangle:=15;
  tabv (xx,yy,sizerectangle);
////////////////////////////////////////////////////////////////////////////////
  textcolor(6);
  gotoxy(46,3);
  write('����');
  xx:=44 ;
  yy:=4;
  sizerectangle:=15;
  tabv (xx,yy,sizerectangle);
////////////////////////////////////////////////////////////////////////////////
textcolor(6);
gotoxy(5,13);
xx:=4;
yy:=14;
sizerectangle:=15;
tabv (xx,yy,sizerectangle);
end;
end.
